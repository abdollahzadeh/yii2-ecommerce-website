<?php
/**
 * Created by PhpStorm.
 * User: Hossein
 * Date: 6/13/2021
 * Time: 4:20 PM
 */
/** @var \common\models\Product $model */
?>

<div class="card h-100">
    <a href="#">
        <img class="card-img-top"
             src="<?php echo $model->getImageUrl() ?>" alt=""> </a>
    <div class="card-body">
        <h4 class="card-title">
            <a href="#"><?=$model->name ?></a>
        </h4>
        <h5><?php echo Yii::$app->formatter->asCurrency($model->price) ?></h5>
        <div class="card-text">
            <?php echo $model->getShortDescription() ?>
        </div>
    </div>
    <div class="card-footer text-right">
        <a href="<?php echo \yii\helpers\Url::to(['/cart/add']) ?>" class="btn btn-primary btn-add-to-cart">
            Add to cart
        </a>
    </div>
</div>