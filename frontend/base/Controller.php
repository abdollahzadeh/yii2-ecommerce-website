<?php
/**
 * Created by PhpStorm.
 * User: Hossein
 * Date: 6/14/2021
 * Time: 7:36 PM
 */

namespace frontend\base;


use common\models\CartItem;

class Controller extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            $cartItems = \Yii::$app->session->get(CartItem::SESSION_KEY, []);
//            echo '<pre>';
//            var_dump($cartItems);exit();

            $sum = 0;
            foreach ($cartItems as $cartItem) {
                $sum += $cartItem['quantity'];

            }
        } else {
            $sum = CartItem::findBySql(
                "SELECT SUM(quantity) FROM cart_items WHERE created_by = :userId", ['userId' => \Yii::$app->user->id]
            )->scalar();
        }

        $this->view->params['cartItemCount'] = $sum;
        return parent::beforeAction($action);
    }
}