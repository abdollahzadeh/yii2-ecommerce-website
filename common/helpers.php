<?php
/**
 * Created by PhpStorm.
 * User: Hossein
 * Date: 6/15/2021
 * Time: 9:22 AM
 */

function isGuest(){
    return Yii::$app->user->isGuest;
}

function currUserId(){
    return Yii::$app->user->id;
}
